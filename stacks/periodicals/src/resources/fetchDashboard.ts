import { GetDashboardResponse } from '../../../../lib/interfaces/endpoints/periodical';
import { Request } from '../../../../lib/lambda/faas';
import { DbContext } from '../../../../lib/lambda/middleware/withDatabase';
import { SessionContext } from '../../../../lib/lambda/middleware/withSession';
import { fetchUserDashboard } from '../services/fetchUserDashboard';

export async function fetchDashboard(
  context: Request<undefined> & DbContext & SessionContext,
): Promise<GetDashboardResponse> {
  if (context.session instanceof Error) {
    throw new Error('You do not appear to be logged in.');
  }

  try {
    const [ periodicals, articles ] = await fetchUserDashboard(context.database, context.session);

    return {
      articles,
      periodicals,
    };
  } catch (e) {
    throw new Error('Could not fetch dashboard info.');
  }
}
