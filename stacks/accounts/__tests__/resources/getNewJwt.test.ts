jest.mock('../../src/dao', () => ({
  getSessionForToken: jest.fn().mockReturnValue(Promise.resolve({ identifier: 'arbitrary_user_id' })),
}));
jest.mock('../../../../lib/lambda/signJwt', () => ({
  signJwt: jest.fn().mockReturnValue('Arbitrary JWT'),
}));

import { getNewJwt } from '../../src/resources/getNewJwt';

const mockContext = {
  body: { refreshToken: 'Arbitrary token' },
  database: {} as any,

  headers: {},
  method: 'POST' as 'POST',
  params: [ '/arbitrary_user_id/jwt', 'arbitrary_user_id' ],
  path: '/arbitrary_user_id/jwt',
  query: null,
};

it('should return a new JWT when passed the correct values', () => {
  return expect(getNewJwt(mockContext)).resolves.toHaveProperty('jwt');
});

it('should error when no refresh token was provided', () => {
  return expect(getNewJwt({
    ...mockContext,
    body: {},
  })).rejects.toEqual(new Error('Please provide a refresh token to receive a new JWT.'));
});

it('should error when no account could be found for a refresh token', () => {
  const mockGetSessionForToken = require.requireMock('../../src/dao').getSessionForToken;
  mockGetSessionForToken.mockReturnValueOnce(Promise.reject(new Error('Arbitrary database error error')));

  return expect(getNewJwt(mockContext)).rejects
    .toEqual(new Error('Something went wrong, please try again.'));
});

it('should error when no user ID to refresh was specified', () => {
  return expect(getNewJwt({
    ...mockContext,
    params: [],
    path: '/',
  })).rejects.toEqual(new Error('No user ID specified.'));
});

it('should error when the refresh token was for an ID other than the one specified', () => {
  const mockGetSessionForToken = require.requireMock('../../src/dao').getSessionForToken;
  mockGetSessionForToken.mockReturnValueOnce({ identifier: 'some_user_id' });

  return expect(getNewJwt({
    ...mockContext,
    params: [ '/some_other_user_id/jwt', 'some_other_user_id' ],
    path: '/some_other_user_id/jwt',
  })).rejects.toEqual(new Error('The refresh token was not valid.'));
});

it('should error when the JWT could not be created', () => {
  const mockSignJwt = require.requireMock('../../../../lib/lambda/signJwt').signJwt;
  mockSignJwt.mockReturnValueOnce(new Error('Arbitrary signing error'));

  return expect(getNewJwt(mockContext)).rejects
    .toEqual(new Error('We are currently experiencing issues, please try again later.'));
});
