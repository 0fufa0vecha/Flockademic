jest.mock('../../src/services/periodical', () => ({
  getArticleUploadUrl: jest.fn().mockReturnValue(Promise.resolve(
    {
      object: {
        toLocation: 'arbitrary_upload_url',
      },
      result: {
        associatedMedia: {
          contentUrl: 'arbitrary_download_url',
          license: 'https://creativecommons.org/licenses/by/4.0/',
          name: 'arbitrary_filename',
        },
      },
    },
  )),
}));
jest.mock('axios');

import {
  FileManager,
  FileManagerProps,
} from '../../src/components/fileManager/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

it('should display just the upload form if no files are known yet', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  expect(toJson(component)).toMatchSnapshot();
});

// tslint:disable-next-line:max-line-length
it('should assume just a single file and thus only show the first one in the files prop, since only single files are supported at the moment', () => {
  const files = [
    { name: 'some_filename', contentUrl: 'some_url' },
    { name: 'arbitrary_filename', contentUrl: 'arbitrary_url' },
  ];
  const component = shallow(<FileManager articleId="arbitrary_id" files={files}/>);

  expect(component.find('[href="some_url"]')).toExist();
  expect(component.find('[href="some_url"]').text()).toBe('some_filename');
});

it('should display the filename of selected files', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });

  expect(component.find('.file-name').first().text()).toBe('some_filename');
});

it('should not allow uploading without agreeing to the license', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: false } });

  expect(component.find('[type="submit"]').prop('disabled')).toBe(true);
});

it('should allow uploading after agreeing to the license', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  expect(component.find('[type="submit"]').prop('disabled')).toBe(false);
});

it('should allow uploading after agreeing to the license', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  expect(component.find('[type="submit"]').prop('disabled')).toBe(false);
});

it('should not allow uploading when no file to upload has been selected', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  expect(component.find('[type="submit"]').prop('disabled')).toBe(true);
});

it('should not allow uploading when multiple files have been selected to upload', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [
        { name: 'arbitrary_filename' },
        { name: 'arbitrary_other_filename' },
      ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  expect(component.find('[type="submit"]').prop('disabled')).toBe(true);
});

it('should block uploading when no file has been selected to upload', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });
  expect(component.state('isUploading')).toBe(false);
});

it('should block uploading when the user does not agree to the license', () => {
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: false } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });
  expect(component.state('isUploading')).toBe(false);
});

it('should fetch an upload URL after submitting the form with valid values', () => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  const component = shallow(<FileManager articleId="some_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  expect(mockedPeriodicalService.getArticleUploadUrl.mock.calls.length).toBe(1);
  expect(mockedPeriodicalService.getArticleUploadUrl.mock.calls[0][0]).toBe('some_id');
  expect(mockedPeriodicalService.getArticleUploadUrl.mock.calls[0][1]).toBe('some_filename');
  expect(mockedPeriodicalService.getArticleUploadUrl.mock.calls[0][2]).toBe(true);
});

it('should display an error message when an upload URL could not be obtained', (done) => {
  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getArticleUploadUrl.mockReturnValueOnce(Promise.reject('Arbitrary error'));
  const component = shallow(<FileManager articleId="some_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();
    expect(component.find('.message.is-danger').text())
      .toBe('Something went wrong uploading your document, please try again.');

    done();
  });
});

it('should display an error message if the connection to S3 failed', (done) => {
  const mockedAxios = require.requireMock('axios');
  mockedAxios.put.mockReturnValueOnce(Promise.reject('Arbitrary error'));

  const component = shallow(<FileManager articleId="arbitrary_id" files={[]}/>);

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'arbitrary_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();
    expect(component.find('.message.is-danger').text())
      .toBe('Something went wrong uploading your document, please try again.');

    done();
  });
});

it('should call the onUpload callback after upload, if set', (done) => {
  const callback = jest.fn();
  const component = shallow(<FileManager articleId="arbitrary_id" files={[]} onUpload={callback}/>);

  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getArticleUploadUrl.mockReturnValueOnce(Promise.resolve({
    object: {
      toLocation: 'arbitrary_upload_url',
    },
    result: {
      associatedMedia: {
        contentUrl: 'some_download_url',
        license: 'https://creativecommons.org/licenses/by/4.0/',
        name: 'arbitrary_filename',
      },
    },
  }));

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    expect(callback.mock.calls.length).toBe(1);
    expect(callback.mock.calls[0][0]).toEqual({
      contentUrl: 'some_download_url',
      license: 'https://creativecommons.org/licenses/by/4.0/',
      name: 'some_filename',
    });

    done();
  });
});

it('should display the uploaded file after upload', (done) => {
  const files = [
    { name: 'some_filename', contentUrl: 'some_url' },
    { name: 'arbitrary_filename', contentUrl: 'arbitrary_url' },
  ];
  const component = shallow(<FileManager articleId="arbitrary_id" files={files}/>);

  const mockedPeriodicalService = require.requireMock('../../src/services/periodical');
  mockedPeriodicalService.getArticleUploadUrl.mockReturnValueOnce(Promise.resolve({
    object: {
      toLocation: 'arbitrary_upload_url',
    },
    result: {
      associatedMedia: {
        contentUrl: 'some_other_url',
        license: 'https://creativecommons.org/licenses/by/4.0/',
        name: 'arbitrary_filename',
      },
    },
  }));

  component.find('input[type="file"]').simulate('change', {
    target: {
      files: [ { name: 'some_other_filename' } ],
    },
  });
  component.find('input[type="checkbox"]').simulate('change', { target: { checked: true } });

  component.find('form').simulate('submit', { preventDefault: jest.fn() });

  setImmediate(() => {
    component.update();
    expect(component.find('[href="some_other_url"]')).toExist();
    expect(component.find('[href="some_other_url"]').text()).toBe('some_other_filename');

    done();
  });
});
